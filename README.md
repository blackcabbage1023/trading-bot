1.

Quantopian is a company providing hosted platform for building and executing trading strategies
Zipline is one of its trading library, where users can find trading strategy code. Zipline also helps the backtest and live trading at Quantopian

2.

The set slippage model helps calculate the impact of the order on the security price
using set_splippage, we can limit the volume of the order 

3.

The strategy of the handle_data function is to buy all the stocks in the list that we have not bought. Once the stock is bought, we will hold it.

4. 

The function is use to backtest. and thus not critial for the strategy







Hello world!
# Header 1
Under header 1

## Header 2
Under header 2

### Header 3
Under header 3

# Link
You can access to [Facebook](www.facebook.com)

# Code Formatting
```py

def function():
    print('Hello World')

```
# Ordered List
1. List 1
2. List 2
3. List 3

# Unordered List
- List
- List
- List

# Another Unordered List
1. List
1. List
1. List
